package com.xuecheng.generator;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Arrays;

/**
 * MyBatis-Plus 代码生成类
 */
public class ContentCodeGenerator {

	// TODO 修改服务名以及数据表名
	private static final String SERVICE_NAME = "content";

	//数据库账号
	private static final String DATA_SOURCE_USER_NAME  = "root";
	//数据库密码
	private static final String DATA_SOURCE_PASSWORD  = "mysql";
	//生成的表
	private static final String[] TABLE_NAMES = new String[]{
//			"mq_message",
//			"mq_message_history"
			"course_base",
			"course_market",
			"course_teacher",
			"course_category",
			"teachplan",
			"teachplan_media",
			"course_publish",
			"course_publish_pre"

	};

	// TODO 默认生成entity，需要生成DTO修改此变量
	// 一般情况下要先生成 DTO类 然后修改此参数再生成 PO 类。
	private static final Boolean IS_DTO = false;

	public static void main(String[] args) {
		// 代码生成器
		AutoGenerator mpg = new AutoGenerator();
		// 选择 freemarker 引擎，默认 Velocity
		mpg.setTemplateEngine(new FreemarkerTemplateEngine());
		// 全局配置
		GlobalConfig gc = new GlobalConfig();
		gc.setFileOverride(true);
		//生成路径
		gc.setOutputDir(System.getProperty("user.dir") + "/xuecheng-plus-generator/src/main/java");
		gc.setAuthor("itcast");
		gc.setOpen(false);
		gc.setSwagger2(false);
		gc.setServiceName("%sService");
        gc.setBaseResultMap(true);
        gc.setBaseColumnList(true);

		if (IS_DTO) {
			gc.setSwagger2(true);
			gc.setEntityName("%sDTO");
		}
		mpg.setGlobalConfig(gc);

		//数据库配置
		DataSourceConfig dsc = new DataSourceConfig();
		dsc.setDbType(DbType.MYSQL);
		dsc.setUrl("jdbc:mysql://192.168.101.65:3306/xc402_" + SERVICE_NAME
				+ "?serverTimezone=UTC&useUnicode=true&useSSL=false&characterEncoding=utf8");
//		dsc.setDriverName("com.mysql.jdbc.Driver");
		dsc.setDriverName("com.mysql.cj.jdbc.Driver");
		dsc.setUsername(DATA_SOURCE_USER_NAME);
		dsc.setPassword(DATA_SOURCE_PASSWORD);
		mpg.setDataSource(dsc);

		//包配置
		PackageConfig pc = new PackageConfig();
		pc.setModuleName(SERVICE_NAME);
		pc.setParent("com.xuecheng");

		pc.setServiceImpl("service.impl");
		pc.setXml("mapper");
		pc.setEntity("model.po");
		mpg.setPackageInfo(pc);


		// 设置模板
		TemplateConfig tc = new TemplateConfig();
		mpg.setTemplate(tc);

		//策略配置
		StrategyConfig strategy = new StrategyConfig();
		//设置数据库表名命名策略为下划线转驼峰命名，即将数据库表名中的下划线风格转换为驼峰命名风格。
		strategy.setNaming(NamingStrategy.underline_to_camel);
		//设置数据库表字段命名策略为下划线转驼峰命名，即将数据库表字段名中的下划线风格转换为驼峰命名风格。
		strategy.setColumnNaming(NamingStrategy.underline_to_camel);
		//配置实体类是否使用 Lombok 注解来简化代码，这样可以通过注解方式来自动生成 getter、setter、toString 等方法。
		strategy.setEntityLombokModel(true);
		//设置生成的 Controller 类使用 @RestController 注解，表示生成的 Controller 类是 REST 风格的控制器。
		strategy.setRestControllerStyle(true);
		//指定需要生成代码的表名，可以是一个数组或者字符串，表示只生成指定表的代码。
		strategy.setInclude(TABLE_NAMES);
		//Controller 类中的路径变量是否使用驼峰命名风格，默认为 false，此处设置为 true 表示使用连字符风格。 @RequestMapping("courseBase")
		strategy.setControllerMappingHyphenStyle(true);
		//设置生成的实体类和数据库表名的前缀，通常用于区分不同模块或业务线的表。@TableName("course_market") @TableName("course_base")
		strategy.setTablePrefix(pc.getModuleName() + "_");
		// Boolean类型字段是否移除is前缀处理
		strategy.setEntityBooleanColumnRemoveIsPrefix(true);
		//再次设置生成的 Controller 类使用 @RestController 注解，确保 REST 风格的控制器。
		strategy.setRestControllerStyle(true);

		// 自动填充字段配置
		strategy.setTableFillList(Arrays.asList(
				new TableFill("create_date", FieldFill.INSERT),
				new TableFill("change_date", FieldFill.INSERT_UPDATE),
				new TableFill("modify_date", FieldFill.UPDATE)
		));
		mpg.setStrategy(strategy);

		mpg.execute();
	}

}
