package com.xuecheng.service.impl;

import com.alibaba.fastjson.JSON;
import com.xuecheng.base.exception.XueChengPlusException;
import com.xuecheng.learning.config.PayNotifyConfig;
import com.xuecheng.messagesdk.model.po.MqMessage;
import com.xuecheng.messagesdk.service.MqMessageService;
import com.xuecheng.service.MyCourseTablesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ReceivePayNotifyService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    MqMessageService mqMessageService;

    @Autowired
    MyCourseTablesService myCourseTablesService;


    //监听消息队列接收支付结果通知 只要队列当中有消息就可以监听到消息进行消息的接收
    @RabbitListener(queues = PayNotifyConfig.PAYNOTIFY_QUEUE)
    public void receive(Message message) {
        //因为消费端的业务逻辑一旦抛出异常就会执行回调方法进行从消息表再次取出这个失败的消息进行业务逻辑的重试
        //一旦失败就会立即取出这个失败的消息进行执行 所以为了不要让它迅速的重新执行所以进行了睡眠
        //为了避免消息被频繁重新消费（重复执行），在捕获到异常后进行了 5 秒的休眠操作。
        //通过让消费者线程休眠一段时间，可以让消费端稍作延迟再重新消费消息，避免出现异常导致频繁消费失败的情况，
        //同时也有利于降低系统负载和提高稳定性。在这段休眠时间内，系统有机会进行一些清理工作或者等待可能的异常情况得到解决，确保消息能够被正确地处理。
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        //获取消息
        MqMessage mqMessage = JSON.parseObject(message.getBody(), MqMessage.class);
        log.debug("学习中心服务接收支付结果:{}", mqMessage);

        //消息类型
        String messageType = mqMessage.getMessageType();
        //订单类型,60201表示购买课程
        String businessKey2 = mqMessage.getBusinessKey2();
        //这里只处理支付结果通知
        if (PayNotifyConfig.MESSAGE_TYPE.equals(messageType) && "60201".equals(businessKey2)) {
            //选课记录id 因为要更新选课的支付状态 所以在消息的发送方需要把选课记录的id发送过来 根据这个id查询出选课记录然后对这条选课记录的支付状态进行修改
            String choosecourseId = mqMessage.getBusinessKey1();
            //添加选课
            boolean b = myCourseTablesService.saveChooseCourseStauts(choosecourseId);
            if(!b){
                //添加选课失败，抛出异常，消息重回队列
                XueChengPlusException.cast("收到支付结果，添加选课失败");
            }
        }


    }


}
