package com.xuecheng.service;

import com.xuecheng.base.model.PageResult;
import com.xuecheng.learning.model.dto.MyCourseTableParams;
import com.xuecheng.learning.model.dto.XcChooseCourseDto;
import com.xuecheng.learning.model.dto.XcCourseTablesDto;
import com.xuecheng.learning.model.po.XcCourseTables;

public interface MyCourseTablesService {
    XcChooseCourseDto addChooseCourse(String userId, Long courseId);

    public XcCourseTablesDto getLearningStatus(String userId, Long courseId);

    public boolean saveChooseCourseStauts(String chooseCourseId);

    public PageResult<XcCourseTables> mycourestabls(MyCourseTableParams params);
}
