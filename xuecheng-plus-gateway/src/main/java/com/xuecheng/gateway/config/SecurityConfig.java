package com.xuecheng.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

/**
 * @description 安全配置类
 * @author Mr.M
 * @date 2022/9/27 12:07
 * @version 1.0
 */
 @EnableWebFluxSecurity
 @Configuration
 public class SecurityConfig {


  //安全拦截配置  对所有路径放行，并要求除放行的路径外的其他请求需要认证。
  @Bean
  public SecurityWebFilterChain webFluxSecurityFilterChain(ServerHttpSecurity http) {

   return http.authorizeExchange()
           //表示对所有路径放行，不进行权限验证。
           .pathMatchers("/**").permitAll()
           //表示对除了之前指定路径以外的所有请求都需要经过认证。
           .anyExchange().authenticated()
           .and().csrf().disable().build();
  }


 }
