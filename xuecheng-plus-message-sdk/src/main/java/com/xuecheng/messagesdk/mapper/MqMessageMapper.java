package com.xuecheng.messagesdk.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xuecheng.messagesdk.model.po.MqMessage;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author itcast
 */
public interface MqMessageMapper extends BaseMapper<MqMessage> {

    //t.state='0'保证了幂等性 因为某一个服务的任务执行成功后会修改此状态 则下次就领取不到已经执行过的任务了
    //t.message_type=#{messageType}表示要执行任务的服务是谁 因为在消息表中可能存在不同服务的任务对提高发布课程的搜索效率有2个服务需要从这里面领取任务
    //如内容管理服务生成静态页面需要从这里领取任务则需要传过来服务的名称也就是消息的类型
    @Select("SELECT t.* FROM mq_message t WHERE t.id % #{shardTotal} = #{shardindex} and t.state='0' and t.message_type=#{messageType} limit #{count}")
    List<MqMessage> selectListByShardIndex(@Param("shardTotal") int shardTotal, @Param("shardindex") int shardindex, @Param("messageType") String messageType,@Param("count") int count);

}
