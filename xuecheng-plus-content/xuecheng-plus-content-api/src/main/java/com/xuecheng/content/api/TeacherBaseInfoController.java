package com.xuecheng.content.api;

import com.xuecheng.content.model.po.CourseTeacher;
import com.xuecheng.content.service.TeacherBaseInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "教师信息管理接口")
@Slf4j
public class TeacherBaseInfoController {
    @Autowired
    private TeacherBaseInfoService teacherBaseInfoService;

    @GetMapping("/courseTeacher/list/{courseId}")
    @ApiOperation("查询教师接口")
    public List<CourseTeacher> getCourseTeacherList(@PathVariable Long courseId){
        List<CourseTeacher> courseTeachers = teacherBaseInfoService.getCourseTeacherList(courseId);
        return courseTeachers;
    }

    @PostMapping("/courseTeacher")
    @ApiOperation("添加或修改教师请求")
    public CourseTeacher saveCourseTeacher(@RequestBody CourseTeacher courseTeacher){
        CourseTeacher courseTeacher1 = teacherBaseInfoService.saveCourseTeacher(courseTeacher);
        return courseTeacher1;
    }

    @DeleteMapping("/courseTeacher/course/{courseId}/{teacherId}")
    @ApiOperation("删除教师")
    public void deleteCourseTeacher(@PathVariable Long courseId,@PathVariable Long teacherId){
        teacherBaseInfoService.deleteCourseTeacher(courseId,teacherId);
    }
}
