package com.xuecheng.content.api;

import com.xuecheng.content.model.dto.BindTeachplanMediaDto;
import com.xuecheng.content.model.dto.SaveTeachplanDto;
import com.xuecheng.content.model.dto.TeachplanDto;
import com.xuecheng.content.model.po.Teachplan;
import com.xuecheng.content.model.po.TeachplanMedia;
import com.xuecheng.content.service.TeachplanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value = "课程计划编辑接口",tags = "课程计划编辑接口")
public class TeachplanController {
    @Autowired
    private TeachplanService teachplanService;

    @GetMapping("/teachplan/{courseId}/tree-nodes")
    @ApiOperation("查询课程计划树形结构")
    public List<TeachplanDto> getTreeNodes(@PathVariable Long courseId){
        List<TeachplanDto> teachplanTree = teachplanService.findTeachplanTree(courseId);
        return teachplanTree;
    }

    @PostMapping("/teachplan")
    @ApiOperation("新增或修改课程计划")
    public void saveTeachplan(@RequestBody SaveTeachplanDto saveTeachplanDto){
        teachplanService.saveTeachplan(saveTeachplanDto);
    }


    @DeleteMapping("/teachplan/{teachplanId}")
    @ApiOperation("删除课程计划")
    public void deleteTeachplanByTeachplanId(@PathVariable Long teachplanId){
        teachplanService.deleteTeachplanByTeachplanId(teachplanId);
    }

    @PostMapping("teachplan/{moveType}/{teachplanId}")
    @ApiOperation("课程计划上下移动")
    public void moveOrDown(@PathVariable String moveType ,@PathVariable Long teachplanId){
        teachplanService.moveOrDown(moveType,teachplanId);
    }

    @ApiOperation(value = "课程计划和媒资信息绑定")
    @PostMapping("/teachplan/association/media")
    public TeachplanMedia associationMedia(@RequestBody BindTeachplanMediaDto bindTeachplanMediaDto){
        TeachplanMedia teachplanMedia = teachplanService.associationMedia(bindTeachplanMediaDto);
        return teachplanMedia;
    }

    @ApiOperation("媒资绑定的解除")
    @DeleteMapping("/teachplan/association/media/{teachPlanId}/{mediaId}")
    public void deleteAssociationMedia(@PathVariable Long teachPlanId, @PathVariable String mediaId){
        teachplanService.deleteAssociationMedia(teachPlanId,mediaId);

    }

    @ApiOperation("课程计划查询")
    @PostMapping("/teachplan/{teachplanId}")
    public Teachplan getTeachplan(@PathVariable Long teachplanId) {
        return teachplanService.getTeachplan(teachplanId);
    }


}
