package com.xuecheng.content.api;

import com.alibaba.fastjson.JSON;
import com.xuecheng.content.model.dto.CourseBaseInfoDto;
import com.xuecheng.content.model.dto.CoursePreviewDto;
import com.xuecheng.content.model.dto.TeachplanDto;
import com.xuecheng.content.model.po.CoursePublish;
import com.xuecheng.content.service.CoursePublishService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;


@Controller
//返回里面有用controller
public class CoursePublishController {

    @Autowired
    private CoursePublishService coursePublishService;

    @GetMapping("/coursepreview/{courseId}")
    @ApiOperation("返回数据模型给freemaker模板引擎进行数据的渲染")
    //返回静态页面给前端 这是课程预览的 另一个是课程发布的
    public ModelAndView preview(@PathVariable("courseId") Long courseId){
        CoursePreviewDto coursePreviewInfo = coursePublishService.getCoursePreviewInfo(courseId);
        //ModelAndView就是包括模型和视图 将来返回给浏览器返回哪个视图和模型可以通过这个ModelAndView来指定
        ModelAndView modelAndView = new ModelAndView();
        //指定数据模型 代码中的指定模型就是前端定义的数据模型用来绑定数据的
        modelAndView.addObject("model",coursePreviewInfo);
        //指定模板  根据视图名称course_template+ftl 找到模板
        modelAndView.setViewName("course_template");
        //这里就是返回的模板+数据渲染的html页面
        return modelAndView;
    }

    @ResponseBody
    //由于前面有页面 这里有json所以这里要加ResponseBody
    @PostMapping("/courseaudit/commit/{courseId}")
    @ApiOperation("提交审核状态")
    public void commitAudit(@PathVariable Long courseId){
        Long companyId = 1232141425L;
        coursePublishService.commitAudit(companyId,courseId);
    }

    @ApiOperation("课程发布")
    @ResponseBody
    @PostMapping ("/coursepublish/{courseId}")
    public void coursepublish(@PathVariable("courseId") Long courseId){
        Long companyId = 1232141425L;
        coursePublishService.publish(companyId,courseId);
    }

    //在学习中心服务远程调用这个方法查询课程发布的信息 从而确定改课程是收费的还是免费的
    @ApiOperation("查询课程发布信息")
    @ResponseBody
    @GetMapping("/r/coursepublish/{courseId}")
    public CoursePublish getCoursepublish(@PathVariable("courseId") Long courseId) {
        CoursePublish coursePublish = coursePublishService.getCoursePublish(courseId);
        return coursePublish;
    }

    //这个是从缓存里面获取课程发布的信息
    @ApiOperation("获取课程发布信息")
    @ResponseBody
    @GetMapping("/course/whole/{courseId}")
    public CoursePreviewDto getCoursePublish(@PathVariable("courseId") Long courseId) {
        CoursePreviewDto coursePreviewDto = new CoursePreviewDto();
        //直接从数据库查询发布课程的信息
//        CoursePublish coursePublish = coursePublishService.getCoursePublish(courseId);
        //先从缓存中查询发布课程的信息没有在查询数据库然后再把信息放入缓存
        CoursePublish coursePublish = coursePublishService.getCoursePublishCache(courseId);
        if(coursePublish == null){
            return coursePreviewDto;
        }
        CourseBaseInfoDto courseBaseInfoDto = new CourseBaseInfoDto();
        BeanUtils.copyProperties(coursePublish,courseBaseInfoDto);

        String teachplanJson = coursePublish.getTeachplan();
        List<TeachplanDto> teachplanDtos = JSON.parseArray(teachplanJson, TeachplanDto.class);

        coursePreviewDto.setCourseBase(courseBaseInfoDto);
        coursePreviewDto.setTeachplans(teachplanDtos);

        return coursePreviewDto;
    }

}