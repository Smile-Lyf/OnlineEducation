package com.xuecheng.content.model.dto;

import com.xuecheng.content.model.po.Teachplan;
import com.xuecheng.content.model.po.TeachplanMedia;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
//这个类对应于json数据结构的解释就是 首先最外层是一个TeachplanDto有着基础的Teachplan属性以及外加teachplanMedia和teachPlanTreeNodes属性
//最外层的TeachplanDto的teachplanMedia是null的 teachPlanTreeNodes里面又嵌套一个TeachplanDto属性 所以teachPlanTreeNodes不是空的
//对于外层的teachPlanTreeNodes包裹的TeachplanDto属性的teachPlanTreeNodes是空的 teachplanMedia属性是有值的
//最大的那个表示大章节 teachPlanTreeNodes表示小章节 teachPlanTreeNodes下的teachplanMedia表示小章节下面的关联的视频 视频只在小章节关联了
public class TeachplanDto extends Teachplan {
    //课程计划关联的媒资信息
    TeachplanMedia teachplanMedia;

    //子结点
    List<TeachplanDto> teachPlanTreeNodes;
}
