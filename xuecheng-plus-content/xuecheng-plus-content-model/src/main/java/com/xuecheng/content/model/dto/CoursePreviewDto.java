package com.xuecheng.content.model.dto;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @description 课程预览数据模型
 * @author Mr.M
 * @date 2022/9/16 15:03
 * @version 1.0
 */
@Data
@ToString
public class CoursePreviewDto {
    //数据模型是根据课程的预览表进行生成的 课程的预览表是四张表的结合 课程营销信息表 课程基本信息表 课程师资表 课程计划表
    //课程基本信息,课程营销信息
    CourseBaseInfoDto courseBase;


    //课程计划信息
    List<TeachplanDto> teachplans;

    //师资信息暂时不加...


}
