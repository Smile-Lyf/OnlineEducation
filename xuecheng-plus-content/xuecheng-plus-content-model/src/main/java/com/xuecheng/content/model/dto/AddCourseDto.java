package com.xuecheng.content.model.dto;

import com.xuecheng.base.exception.ValidationGroups;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * @description 添加课程dto
 * @author Mr.M
 * @date 2022/9/7 17:40
 * @version 1.0
 */
@Data
@ApiModel(value="AddCourseDto", description="新增课程基本信息")
public class AddCourseDto {

 //这里的message的作用是在校验出错抛出异常的时候可以进行全局异常处理器进行捕获这个异常然后拿出message信息进行传给前端进行页面的提示
 //分组校验的作用是在多个接口使用同一个模型类 也就是如果新增课程用的这个 修改课程也用的这个模型类 且这两个接口对这个属性的校验规则不一样则需要加上分组进行区分
 //也就是新增课程走一个组 修改课程走一个组 相当于走新增课程的时候id不在这个对象里面 修改课程的时候id在这个对象里面 且在controller上也要加上分组才能知道此时要走的是哪一个组
 //这个组的定义是在base工程下的ValidationGroups类里面自定义的
 //如果不加分组添加两个模型类也是可以进行区分的 添加课程用一个模型类 修改课程用一个模型类就好了
 //整体的流程就是当新增课程的时候在controller的请求参数里面加上注解和组别 指明需要走哪个组 然后进到这个类里面
 // 如果是新增课程的话 走的是这个组@NotEmpty(message = "新增课程名称不能为空",groups = {ValidationGroups.Inster.class})这个组进行生效 校验的也是这个
 @NotEmpty(message = "新增课程名称不能为空",groups = {ValidationGroups.Inster.class})
 @NotEmpty(message = "修改课程名称不能为空",groups = {ValidationGroups.Update.class})
 @ApiModelProperty(value = "课程名称", required = true)
 private String name;

 @NotEmpty(message = "适用人群不能为空")
 @Size(message = "适用人群内容过少",min = 10)
 @ApiModelProperty(value = "适用人群", required = true)
 private String users;

 @ApiModelProperty(value = "课程标签")
 private String tags;

 @NotEmpty(message = "课程分类不能为空")
 @ApiModelProperty(value = "大分类", required = true)
 private String mt;

 @NotEmpty(message = "课程分类不能为空")
 @ApiModelProperty(value = "小分类", required = true)
 private String st;

 @NotEmpty(message = "课程等级不能为空")
 @ApiModelProperty(value = "课程等级", required = true)
 private String grade;

 @ApiModelProperty(value = "教学模式（普通，录播，直播等）", required = true)
 private String teachmode;

 @ApiModelProperty(value = "课程介绍")
 private String description;

 @ApiModelProperty(value = "课程图片", required = true)
 private String pic;

 @NotEmpty(message = "收费规则不能为空")
 @ApiModelProperty(value = "收费规则，对应数据字典", required = true)
 private String charge;

 @ApiModelProperty(value = "价格")
 private Float price;
 @ApiModelProperty(value = "原价")
 private Float originalPrice;


 @ApiModelProperty(value = "qq")
 private String qq;

 @ApiModelProperty(value = "微信")
 private String wechat;
 @ApiModelProperty(value = "电话")
 private String phone;

 @ApiModelProperty(value = "有效期")
 private Integer validDays;
}
