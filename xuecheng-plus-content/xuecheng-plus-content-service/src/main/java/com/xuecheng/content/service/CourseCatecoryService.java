package com.xuecheng.content.service;


import com.xuecheng.content.model.dto.CourseCategoryTreeDto;

import java.util.List;

public interface CourseCatecoryService {

    public List<CourseCategoryTreeDto> queryCatecoryNodes(String id);
}
