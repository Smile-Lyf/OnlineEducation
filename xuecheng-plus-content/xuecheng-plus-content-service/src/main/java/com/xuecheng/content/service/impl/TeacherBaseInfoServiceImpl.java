package com.xuecheng.content.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xuecheng.base.exception.XueChengPlusException;
import com.xuecheng.content.mapper.CourseTeacherMapper;
import com.xuecheng.content.model.po.CourseTeacher;
import com.xuecheng.content.service.TeacherBaseInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
@Service
@Slf4j
public class TeacherBaseInfoServiceImpl implements TeacherBaseInfoService {
    @Autowired
    private CourseTeacherMapper courseTeacherMapper;

    @Override
    public List<CourseTeacher> getCourseTeacherList(Long courseId) {

        List<CourseTeacher> courseTeachers = courseTeacherMapper.selectByCourseId(courseId);
        return courseTeachers;
    }

    @Override
    public CourseTeacher saveCourseTeacher(CourseTeacher courseTeacher) {
        Long courseTeacherId = courseTeacher.getId();
        if(courseTeacherId == null){
            CourseTeacher teacher = new CourseTeacher();
            BeanUtils.copyProperties(courseTeacher,teacher);
            teacher.setCreateDate(LocalDateTime.now());
            int i = courseTeacherMapper.insert(teacher);
            if(i<=0){
                XueChengPlusException.cast("添加教师失败");
            }
            return getCourseTeacher(teacher);
        }else {
            //查出来原来的 然后把原来的替换掉 就是修改操作了
            CourseTeacher teacher = courseTeacherMapper.selectById(courseTeacherId);
            BeanUtils.copyProperties(courseTeacher,teacher);
            int i = courseTeacherMapper.updateById(teacher);
            if(i<=0){
                XueChengPlusException.cast("修改教师失败");
            }
            return getCourseTeacher(teacher);
        }

    }

    @Override
    public void deleteCourseTeacher(Long courseId, Long teacherId) {
        LambdaQueryWrapper<CourseTeacher> courseTeacherLambdaQueryWrapper = new LambdaQueryWrapper<CourseTeacher>()
                .eq(CourseTeacher::getCourseId, courseId)
                .eq(CourseTeacher::getId, teacherId);
        int flag = courseTeacherMapper.delete(courseTeacherLambdaQueryWrapper);
        if(flag<=0){
            XueChengPlusException.cast("删除教师失败");
        }
    }

    public CourseTeacher getCourseTeacher(CourseTeacher courseTeacher) {
        return courseTeacherMapper.selectById(courseTeacher.getId());
    }

}
