package com.xuecheng.content.feignclient;

import com.xuecheng.content.config.MultipartSupportConfig;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description 媒资管理服务远程接口
 * @author Mr.M
 * @date 2024/2/03 20:29
 * @version 1.0
 */
//MultipartSupportConfig支持远程调用的时候传输文件
//fallbackFactory降级的方案
//value指定指定了要调用的服务的名称或者 URL 地址
@FeignClient(value = "media-api",configuration = MultipartSupportConfig.class
                ,fallbackFactory = MediaServiceClientFallbackFactory.class)
public interface MediaServiceClient {
    //consumes = MediaType.MULTIPART_FORM_DATA_VALUE 当客户端调用这个接口方法时，必须使用 multipart/form-data 类型的请求，并且携带相应的参数和文件数据来完成文件上传操作。
    @RequestMapping(value = "/media/upload/coursefile",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiOperation("图片上传接口")
    public String upload(@RequestPart("filedata") MultipartFile filedata,
                         @RequestParam(value= "objectName",required=false) String objectName);

}
