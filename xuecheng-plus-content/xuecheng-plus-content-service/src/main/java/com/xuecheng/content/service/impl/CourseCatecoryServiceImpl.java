package com.xuecheng.content.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xuecheng.content.mapper.CourseCategoryMapper;
import com.xuecheng.content.model.dto.CourseCategoryTreeDto;
import com.xuecheng.content.model.po.CourseCategory;
import com.xuecheng.content.service.CourseCatecoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CourseCatecoryServiceImpl extends ServiceImpl<CourseCategoryMapper, CourseCategory> implements CourseCatecoryService {

    @Autowired
    private CourseCategoryMapper courseCategoryMapper;

    //CourseCategoryTreeDto相当于在CourseCategory表上添加了一个字段childrenTreeNodes
    //而且childrenTreeNodes是一个集合 这个集合里面放着CourseCategoryTreeDto
    //首先对于树形结构的表 需要先找出父节点的那条对象也就是1-1的对象 然后再把1-1的对象的子节点的对象（如1-1-1 1-1-2）放到1-1对象的childrenTreeNodes集合里面
    @Override
    public List<CourseCategoryTreeDto> queryCatecoryNodes(String id) {
        List<CourseCategoryTreeDto> courseCategoryTreeDtos = courseCategoryMapper.selectTreeNodes(id);
        //这一步是list转成map
        Map<String, CourseCategoryTreeDto> mapTemp = courseCategoryTreeDtos.stream()
                .filter(item -> !id.equals(item.getId()))
                .collect(Collectors.toMap(key -> key.getId(), value -> value, (key1, key2) -> key2));
        //courseCategoryList是最终要返回给前端的 也就是最外层是1-1 或 1-10的对象的数据 其中的属性childrenTreeNodes放的就是1-1-1或则1-1-2
        // 内层是1-1父节点的子节点的1-1-1或1-1-1对象的数据 其中属性childrenTreeNodes值为null
        ArrayList<CourseCategoryTreeDto> courseCategoryList = new ArrayList<>();
        //这一步使用stream流对map进行遍历
        courseCategoryTreeDtos.stream()
                .filter(item -> !id.equals(item.getId()))
                .forEach(item-> {
                    //查看qq截图进行分析 如果遍历到1-1这条数据的对象的父id为1 也就是第一节点被过滤掉的那个节点 就把这个数据放到courseCategoryList外层
                    //其实就是把1-1或则1-10这些第二级节点的数据放到courseCategoryList外层 其实就是判断是不是二级节点
                    //然后开始往下执行 到了courseCategoryParent != null发现1-1不符合这个条件因为它的父节点被过滤掉了不存在 所以遍历到了下一个数据1-1-1
                    //又由于1-1-1不符合item.getParentid().equals(id)所以往下走
                    if(item.getParentid().equals(id)){
                        courseCategoryList.add(item);
                    }
                    //拿到当前子节点的父节点 例如当前遍历到了1-1-1 则父id为1-1 所以拿到了1-1的这条记录
                    CourseCategoryTreeDto courseCategoryParent = mapTemp.get(item.getParentid());
                    if(courseCategoryParent != null){
                        //看qq的截图 对于ChildrenTreeNodes里面的ChildrenTreeNodes是null
                        //一开始遍历到1-1-1的时候发现其父节点1-1的tChildrenTreeNodes的属性是空的 所以创建一个集合便于1-1-1存到1-1的tChildrenTreeNodes属性中
                        if(courseCategoryParent.getChildrenTreeNodes() == null){
                            courseCategoryParent.setChildrenTreeNodes(new ArrayList<CourseCategoryTreeDto>());
                        }
                        //例如当前遍历到了1-1-1 则父id为1-1 则在父节点的ChildrenTreeNodes里面把1-1-1放进去
                        courseCategoryParent.getChildrenTreeNodes().add(item);
                    }
                });
        return courseCategoryList;
    }
}
