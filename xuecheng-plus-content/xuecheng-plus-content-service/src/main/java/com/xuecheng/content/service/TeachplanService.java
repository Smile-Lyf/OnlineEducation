package com.xuecheng.content.service;

import com.xuecheng.content.model.dto.BindTeachplanMediaDto;
import com.xuecheng.content.model.dto.SaveTeachplanDto;
import com.xuecheng.content.model.dto.TeachplanDto;
import com.xuecheng.content.model.po.Teachplan;
import com.xuecheng.content.model.po.TeachplanMedia;

import java.util.List;

public interface TeachplanService {

  public List<TeachplanDto> findTeachplanTree(Long courseId);

  void saveTeachplan(SaveTeachplanDto saveTeachplanDto);

    void deleteTeachplanByTeachplanId(Long courseId);

    void moveOrDown(String moveType,Long teachplanId);

  /**
   * @description 教学计划绑定媒资
   * @param bindTeachplanMediaDto
   * @return com.xuecheng.content.model.po.TeachplanMedia
   * @author Mr.M
   * @date 2022/9/14 22:20
   */
  public TeachplanMedia associationMedia(BindTeachplanMediaDto bindTeachplanMediaDto);

  void deleteAssociationMedia(Long teachPlanId, String mediaId);

    Teachplan getTeachplan(Long teachplanId);
}
