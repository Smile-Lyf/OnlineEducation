package com.xuecheng.media;

import org.junit.jupiter.api.Test;
import org.springframework.util.DigestUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BigFileTest {
    @Test
    public void testChunk() throws IOException {
        //这是读出文件的地方
        File sourceFile = new File("C:\\Users\\梁渊富\\Videos\\Captures\\服装广告.mp4");
        String chunkFilePath = "D:\\xuecheng-plus-project402\\Vedio\\";
        int chunkSize = 1024 * 1024 * 5;
        //文件的大小除以每块的大小 得到一共需要分成几块
        int chunkNum = (int) Math.ceil(sourceFile.length() * 1.0 / chunkSize);
        //RandomAccessFile表示可以对文件进行读操作或则写操作
        //raf_r这里可以操作读  "r"表示读  从sourceFile源文件进行读操作
        RandomAccessFile raf_r = new RandomAccessFile(sourceFile, "r");
        //bytes就是一个缓存区 临时存取数据用的 可以从缓冲区进行读数据和写数据
        byte[] bytes = new byte[1024];
        for(int i = 0 ; i<chunkNum; i++){
            //这是写入文件的地方
            File chunkFile = new File(chunkFilePath + i);
            //这里可以操作写 "rw"表示写  chunkFile从目标文件进行写操作
            RandomAccessFile raf_rw = new RandomAccessFile(chunkFile, "rw");
            int len = -1;
            //边读边写  读一段就赋值一段给len 读完的时候也写完了
            //这个 while 循环的作用是逐块读取源文件sourceFile的数据到缓存区bytes里面，
            // 每次从缓存区bytes读取一部分数据并写入chunkFile目标文件中，直到源文件全部读取完毕或者目标文件达到指定大小为止
            while ((len = raf_r.read(bytes))!=-1){
                //从缓存区bytes拿出数据对目标文件进行写操作 从缓存区的第0号位置开始取数据 取len个长度 然后写入目标文件里面 len长度就是在进行读数据往缓冲区放入的长度
                raf_rw.write(bytes,0,len);
                //通过判断 chunkFile 文件的长度是否达到了指定的 chunkSize，如果达到了，则跳出循环
                if(chunkFile.length()>=chunkSize){
                    break;
                }
            }
            raf_rw.close();
        }
        raf_r.close();
    }

    @Test
    public void testMerge() throws IOException {
        //块文件的目录
        File chunkFolder = new File("D:\\xuecheng-plus-project402\\Vedio\\");
        //源文件
        File sourceFile = new File("C:\\Users\\梁渊富\\Videos\\Captures\\服装广告.mp4");
        //合并文件的目录
        File mergeFile = new File("C:\\Users\\梁渊富\\Videos\\Captures\\服装广告_2.mp4");
        //取出所有的分块文件 是一个数组
        File[] files = chunkFolder.listFiles();

        //将数组转成list
        List<File> fileList = Arrays.asList(files);

        //对 分块进行排序 从0号分块进行合并 排完序号以后fileList这个分块文件的集合就是有序的了
        Collections.sort(fileList, new Comparator<File>() {
            @Override
            //compare比较器 o1.getName()转成整形就是块号（因为文件名就是序号 文件的名称就是按块号取的） o1-o2是升序 0 1 2 3
            public int compare(File o1, File o2) {
                return Integer.parseInt(o1.getName())-Integer.parseInt(o2.getName());
            }
        });

        //当while写完以后mergeFile里面放的就是分块文件合并后的视频文件了
        RandomAccessFile raf_rw = new RandomAccessFile(mergeFile, "rw");

        byte[] bytes = new byte[1024];

        for(File file: fileList){
            RandomAccessFile raf_r = new RandomAccessFile(file, "r");
            int len=-1;
            while((len = raf_r.read(bytes))!=-1){
                raf_rw.write(bytes,0,len);
            }
            raf_r.close();
        }

        raf_rw.close();
        //进行文件的md5值比较
        FileInputStream fileInputStream_source = new FileInputStream(sourceFile);
        FileInputStream fileInputStream_merge = new FileInputStream(mergeFile);
        String md5_source = DigestUtils.md5DigestAsHex(fileInputStream_source);
        String md5_merge = DigestUtils.md5DigestAsHex(fileInputStream_merge);
        if(md5_merge.equals(md5_source)){
            System.out.println("文件合并成功");
        }
    }

}
