package com.xuecheng.media.api;

import com.xuecheng.base.model.PageParams;
import com.xuecheng.base.model.PageResult;
import com.xuecheng.media.model.dto.QueryMediaParamsDto;
import com.xuecheng.media.model.dto.UploadFileParamsDto;
import com.xuecheng.media.model.dto.UploadFileResultDto;
import com.xuecheng.media.model.po.MediaFiles;
import com.xuecheng.media.service.MediaFileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @description 媒资文件管理接口
 * @author Mr.M
 * @date 2022/9/6 11:29
 * @version 1.0
 */
 @Api(value = "媒资文件管理接口",tags = "媒资文件管理接口")
 @RestController
public class MediaFilesController {


      @Autowired
      MediaFileService mediaFileService;


     @ApiOperation("媒资列表查询接口")
     @PostMapping("/files")
     public PageResult<MediaFiles> list(PageParams pageParams, @RequestBody QueryMediaParamsDto queryMediaParamsDto){
      Long companyId = 1232141425L;
      return mediaFileService.queryMediaFiels(companyId,pageParams,queryMediaParamsDto);

     }

     @RequestMapping(value = "/upload/coursefile",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
     @ApiOperation("图片和发布课程的静态文件上传接口")
     //视频上传相当与大文件上传 在BigFilesController里面   objectName是在把静态页面上传到Minio所指定的存放位置 如果没有这个参数的话就表示上传的图片按年月日存储
     //filedata代表的是文件的数据 File代表的是文件的路径 可以通过transferTo方法把数据存到指定的文件路径里面
     public UploadFileResultDto upload(@RequestPart("filedata") MultipartFile filedata,
                                       @RequestParam(value= "objectName",required=false) String objectName) throws IOException {
         Long companyId = 1232141425L;

         UploadFileParamsDto uploadFileParamsDto = new UploadFileParamsDto();
         //文件大小
         uploadFileParamsDto.setFileSize(filedata.getSize());
         //图片
         uploadFileParamsDto.setFileType("001001");
         //文件名称
         uploadFileParamsDto.setFilename(filedata.getOriginalFilename());//文件名称

         //创建临时文件
         File tempFile = File.createTempFile("minio", "temp");
         //上传的文件拷贝到临时文件 可以根据临时文件的路径指定为本地的文件的路径 在上传文件到Minio的时候有用到这个参数
         filedata.transferTo(tempFile);
         //文件路径
         String localFilePath = tempFile.getAbsolutePath();

         UploadFileResultDto uploadFileResultDto = mediaFileService.uploadFile(companyId, uploadFileParamsDto, localFilePath,objectName);

         return uploadFileResultDto;
     }

}
