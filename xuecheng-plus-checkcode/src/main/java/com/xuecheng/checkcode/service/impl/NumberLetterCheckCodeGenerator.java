package com.xuecheng.checkcode.service.impl;

import com.xuecheng.checkcode.service.CheckCodeService;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * @version 1.0
 * @description 数字字母生成器
 */
@Component("NumberLetterCheckCodeGenerator")
public class NumberLetterCheckCodeGenerator implements CheckCodeService.CheckCodeGenerator {

    //用于生成code_length的验证码的长度设置的是4位 生成四位验证码 作为redis的value
    @Override
    public String generate(int length) {
        //定义了一个包含大写字母和数字的字符串 str 作为随机字符的候选集合。
        String str="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        //创建了一个 Random 对象 random 用于生成随机数。
        Random random=new Random();
        //创建了一个 StringBuffer 对象 sb 用于存储生成的验证码。
        StringBuffer sb=new StringBuffer();
        for(int i=0;i<length;i++){
            //random.nextInt(36) 生成一个介于 0 到 35（不包括 36）之间的随机整数  number为字符串的下标可以通过下标取出字母
            int number=random.nextInt(36);
            //str.charAt(number) 取得候选字符集合中索引为 number 的字符，并将其添加到 StringBuffer 对象 sb 中  charAt可以通过下标取出字符串中的字母
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }


}
