package com.xuecheng.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.util.Arrays;

/**
 * @author Administrator
 * @version 1.0
 **/
@Configuration
//TokenConfig为令牌策略配置类
public class TokenConfig {

    private String SIGNING_KEY = "mq123";



//    @Bean
//    public TokenStore tokenStore() {
//        //使用内存存储令牌（普通令牌） 把认证服务的生成令牌的方式换成JWT令牌 因为如果采用认证服务生成的令牌资源服务还需要请求认证服务校验令牌 而jwt资源服务可以自己校验令牌
//        return new InMemoryTokenStore();
//    }

    @Autowired
    private JwtAccessTokenConverter accessTokenConverter;

    @Autowired
    TokenStore tokenStore;
    @Bean
    //JWT访问令牌转换器用于处理JWT令牌的编码和解码工作
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(SIGNING_KEY);
        return converter;
    }


    @Bean
    //使用jwt的tokenStore
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    //令牌管理服务  //TokenConfig为令牌策略配置类
    @Bean(name="authorizationServerTokenServicesCustom")
    public AuthorizationServerTokenServices tokenService() {
        DefaultTokenServices service=new DefaultTokenServices();
        service.setSupportRefreshToken(true);//支持刷新令牌
        service.setTokenStore(tokenStore);//令牌存储策略 设置令牌存储策略为之前定义的tokenStore。也就是采用jwt令牌

        //通过这种方式，可以在生成的JWT令牌中添加额外的信息或自定义逻辑，从而定制化令牌的内容
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(accessTokenConverter));
        service.setTokenEnhancer(tokenEnhancerChain);

        service.setAccessTokenValiditySeconds(7200); // 令牌默认有效期2小时 配置令牌的有效期
        service.setRefreshTokenValiditySeconds(259200); // 刷新令牌默认有效期3天
        return service;
    }


}
