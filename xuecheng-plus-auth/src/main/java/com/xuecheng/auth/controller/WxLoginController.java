package com.xuecheng.auth.controller;


import com.xuecheng.ucenter.model.po.XcUser;
import com.xuecheng.ucenter.service.WxAuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@Slf4j
@Controller
public class WxLoginController {

    @Autowired
    private WxAuthService wxAuthService;
    //这是定义接收微信下发授权码的接口
    //用户点击确认授权后 认证服务会下发授权码 然后就会调用这个接口去申请令牌，并且将用户信息写入本项目数据库
    @RequestMapping("/wxLogin")
    public String wxLogin(String code, String state) throws IOException {
        log.debug("微信扫码回调,code:{},state:{}",code,state);
        //请求微信申请令牌，拿到令牌查询用户信息，将用户信息写入本项目数据库
        XcUser xcUser = wxAuthService.wxAuth(code);

        if(xcUser==null){
            return "redirect:http://www.51xuecheng.cn/error.html";
        }
        String username = xcUser.getUsername();
        //会跳到UserServiceImpl 实现微信操作的登录逻辑的判断校验 传过去的username就是loadUserByUsername里面的参数 authType就是AuthParamsDto里面的属性
        //这是从定向到了登录页面了sign.html 自动填写用户的信息然后登录成功进入网页
        return "redirect:http://www.51xuecheng.cn/sign.html?username="+username+"&authType=wx";
    }
}