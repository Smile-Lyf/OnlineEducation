package com.xuecheng.ucenter.service;

import com.xuecheng.ucenter.model.po.XcUser;

public interface WxAuthService {
    //请求微信申请令牌，拿到令牌查询用户信息，将用户信息写入本项目数据库
    public XcUser wxAuth(String code);
}
