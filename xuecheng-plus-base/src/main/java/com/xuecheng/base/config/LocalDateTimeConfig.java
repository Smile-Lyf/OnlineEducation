package com.xuecheng.base.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Configuration
//这段配置主要用于前端和后端数据之间的转换。在一个典型的前后端分离的应用中，
//前端通常发送 JSON 格式的数据到后端，后端则需要将接收到的 JSON 数据转换成后端能够处理的 Java 对象，并且在将处理结果返回给前端时，需要将 Java 对象序列化成 JSON 格式的数据。
public class LocalDateTimeConfig {

    /*
     * 序列化内容
     *   LocalDateTime -> String 将 LocalDateTime 类型转换为 JSONString
     * 服务端返回给客户端内容
     * */
    @Bean
    public LocalDateTimeSerializer localDateTimeSerializer() {
        return new LocalDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    /*
     * 反序列化内容
     *   String -> LocalDateTime 将 JSONString 转换为 LocalDateTime 类型
     * 客户端传入服务端数据
     * */
    @Bean
    public LocalDateTimeDeserializer localDateTimeDeserializer() {
        return new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    //long转JSONString避免精度损失  实现对象到 JSON 数据的转换过程
    //因为数据库拿到的是正确的payNo 而前端拿到的payNo是精度损失的
    //然后支付接口查询接收的参数是String的所以存在Long转成String的精度损失 所以可以写一个框架配置只要Long转成String的时候使用自己的序列化框架
    //在queryPayResultFromAlipay方法里面有用到
    @Bean
    public ObjectMapper jacksonObjectMapper(Jackson2ObjectMapperBuilder builder) {
        ObjectMapper objectMapper = builder.createXmlMapper(false).build();
        //忽略value为null 时 key的输出
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        SimpleModule module = new SimpleModule();
        module.addSerializer(Long.class, ToStringSerializer.instance);
        module.addSerializer(Long.TYPE, ToStringSerializer.instance);
        objectMapper.registerModule(module);
        return objectMapper;
    }


    // 配置 配置了 Jackson2ObjectMapperBuilder，使其在序列化和反序列化 LocalDateTime 类型时使用前面定义的
    // LocalDateTimeSerializer 和 LocalDateTimeDeserializer。
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
        return builder -> {
            builder.serializerByType(LocalDateTime.class, localDateTimeSerializer());
            builder.deserializerByType(LocalDateTime.class, localDateTimeDeserializer());
        };
    }

}