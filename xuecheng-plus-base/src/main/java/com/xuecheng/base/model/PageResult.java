package com.xuecheng.base.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
public class PageResult<T> implements Serializable {

    // 数据列表 分页查询出来的多条数据
    private List<T> items;

    //总记录数
    private long counts;

    //当前页码
    private long page;

    //每页记录数
    private long pageSize;

    //用于封装返回给前端页面的数据 前端页面就可以根据这个PageResult取出对应的分页数据 总记录数 页号 每页大小进行数据的展示
    //这里采用有参构造 就不用一个一个set了 OJ项目没有这个 它是直接定义了一个什么都可以返回的统一类对分页数据的返回 和本项目的RestResponse一样
    public PageResult(List<T> items, long counts, long page, long pageSize) {
        this.items = items;
        this.counts = counts;
        this.page = page;
        this.pageSize = pageSize;
    }



}
