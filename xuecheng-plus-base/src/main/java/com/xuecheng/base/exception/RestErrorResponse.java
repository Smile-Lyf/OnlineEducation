package com.xuecheng.base.exception;

import java.io.Serializable;

public class RestErrorResponse implements Serializable {
    private String errMessage;


    //有参构造 如果采用有参构造直接传参数就可以了
    public RestErrorResponse(String errMessage){
        this.errMessage= errMessage;
    }


    //  这个是get方法 帮助从这个对象中获取错误的信息
    public String getErrMessage() {
        return errMessage;
    }
    //如果要采用这个set方法去赋值的话需要new一个RestErrorResponse
    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }

}
