
package com.xuecheng.base.exception;


/**
 * @description 通用错误信息
 * @version 1.0
 */

public enum CommonError {

	//枚举类
	UNKOWN_ERROR("执行过程异常，请重试。"),
	PARAMS_ERROR("非法参数"),
	OBJECT_NULL("对象为空"),
	QUERY_NULL("查询结果为空"),
	REQUEST_NULL("请求参数为空");

	private String errMessage;

	//有参构造 枚举类的构造方法是私有的，只能在枚举类内部使用 当您声明枚举常量时，构造方法会被隐式调用
	private CommonError( String errMessage) {
		this.errMessage = errMessage;
	}


	//get方法
	public String getErrMessage() {
		return errMessage;
	}
	//set方法
	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}



}
