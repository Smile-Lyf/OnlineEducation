package com.xuecheng.base.exception;

import lombok.Data;

@Data
public class XueChengPlusException extends RuntimeException{
    private String errMessage;
    //无参构造
    public XueChengPlusException() {
        super();
    }
    //有参构造
    public XueChengPlusException(String errMessage) {
        //父类是RuntimeException
        super(errMessage);
        this.errMessage = errMessage;
    }


    //get方法
    public String getErrMessage() {
        return errMessage;
    }

    //CommonError和RestErrorResponse都定义了errMessage属性   XueChengPlusException也定义了errMessage属性

    // 为了方便以后抛出异常也就是调用这个方法也就是可以不需要new throw就可以进行抛出了 因为静态方法可以直接通过.方法名去调用 可以定义一个静态方法
    //如果不抛出我们自己的异常类型的话 前端收到的异常报出500不友好
    public static void cast(String errMessage){
        throw new XueChengPlusException(errMessage);
    }

    //处理CommonError的错误异常信息  这是处理一些通用的异常信息 和XueChengPlusException的cast一样 不过在抛出的时候不是用的自己写的错误信息而是CommonError里面的枚举信息
    //如 XueChengPlusException.cast("课程名称为空");这个是自己写的  XueChengPlusException.cast(error.UNKOWN_ERROR这个是枚举的)
    public static void cast(CommonError error){
        throw new XueChengPlusException(error.getErrMessage());
    }

}
